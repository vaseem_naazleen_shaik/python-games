# PYTHON GAMES

Simple games build using python modules like Turtle, Pygame.

## Snake Game:

 ### Modules used:
 * Turtle.
 * Random.
 * Time.


 ### Rules:

 * The game consists of a snake and food displayed on random position.
 * Our aim is to make snake eat the food and grow it as big as possible.
 * The food is displayed in red colour at random position.
 * Each time snake eats food score increases by 10 and the body of the snake grows by one segment.
 * The game ends if the snake head touches its own body or hits the sides of the window.

 ### Game Window:


 ![snake](/uploads/9fea95378440c3fa0236b7055c3369cd/snake.png)


 # Space Ivaders:

 ### Modules used:

 * Pygame.
 * Random.

 ### Rules:

 * We are travelling in space where there are lots of monsters.
 * Our aim is to keep moving as far as we can and maximise our score.
 * To survive you should kill the monsters by shooting bullets.
 * Everytime you kill a monster your score increments by one.
 * If the monster enters into your line then the game ends.

 ### Movements:
 
 * Left key: to move left side.
 * Right key: to move right side.
 * Space bar: to launch the bullet.

 ### Game window:

 ![space](/uploads/624ebeb691de76d74a9c0765b33d7a22/space.png)

# Matching Game:

## Modules used:
 
* os
* Random
* Pygame
* time

## Rules:

* The game windows consists of different pairs of animal images placed in random at filed position.
* Our aim is to find the same image pairs by memorizing them.
* when you hit the same images then they will be disappeared
* Game ends when you clear the game window

## Movements:

* Use mouse click to click on a image
* press X in game window or Esc in keyboard to exit the game

## Game window 

**Start and clicked two images**

![matching](/uploads/1cb1bfb9b6b932ae29bd9e0dce7b3221/matching.JPG)

**Same images**

![same](/uploads/7f282b9b77bd74bd6530664d75557d6c/same.png)

**displays matched**

![matched](/uploads/1d9f14e79b9febc96d3ed9a2ec8027f9/matched.png)






